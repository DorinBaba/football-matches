﻿using Domain.Contracts;
using System;

namespace Domain.Entities
{
    public abstract class BaseEntity : IIdentifiableEntity
    {
        public Guid Id { get; set; }
    }
}
