﻿using System.Collections.Generic;
using System.Linq;

namespace Domain.Entities
{
    public class Team : BaseEntity
    {
        public string Name { get; set; }
        public ICollection<Player> Players { get; set; } = new List<Player>();
        public ICollection<Match> AttendedMatches1 { get; set; } = new List<Match>();
        public ICollection<Match> AttendedMatches2 { get; set; } = new List<Match>();

        public ICollection<Match> AttendedMatches
        {
            get => AttendedMatches1.Concat(AttendedMatches2).ToList();
        }
    }
}
