﻿using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public class Player : BaseEntity
    {
        public string Name { get; set; }
        public int Number { get; set; }
        public ICollection<Team> Teams { get; set; } = new List<Team>();
    }
}
