﻿using System.Collections.Generic;

namespace Domain.Entities
{
    public class Location : BaseEntity
    {
        public string Name { get; set; }
        public ICollection<Match> Matches { get; set; } = new List<Match>();
    }
}
