﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Entities
{
    public class Match : BaseEntity
    {
        public Guid Team1Id { get; set; }
        public Team Team1 { get; set; }
        public int Team1Score { get; set; }
        
        public Guid Team2Id { get; set; }
        public Team Team2 { get; set; }
        public int Team2Score { get; set; }

        public Guid LocationId { get; set; }
        public Location Location { get; set; }

        public DateTime StartTime { get; set; }
        public int Minutes { get; set; }

        public ICollection<Player> Players
        {
            get => Team1.Players.Concat(Team2.Players).ToList();
        }
    }
}
