﻿using System;

namespace Domain.Contracts
{
    public interface IIdentifiableEntity
    {
        public Guid Id { get; set; }
    }
}
