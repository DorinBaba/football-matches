﻿using System.Collections.Generic;
using System.Linq;

namespace Domain.ExtensionMethods
{
    public static class CollectionExtensionMethods
    {
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> collection)
        {
            return collection == null || collection.Count() == 0;
        }
    }
}
