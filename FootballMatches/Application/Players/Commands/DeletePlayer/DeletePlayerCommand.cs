﻿using Application.Common.CQRS;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Application.Common.Security;
using Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Players.Commands.DeletePlayer
{
    [Authorize(Roles = "Administrator")]
    public class DeletePlayerCommand : IRequest
    {
        public Guid Id { get; set; }
    }

    public class DeletePlayerCommandHandler : BaseCRUDHandler, IRequestHandler<DeletePlayerCommand>
    {
        public DeletePlayerCommandHandler(IApplicationDbContext dbContext) 
            : base(dbContext)
        {
        }

        public async Task<Unit> Handle(DeletePlayerCommand request, CancellationToken cancellationToken)
        {
            Player player = await _dbContext.Players
                .FirstOrDefaultAsync(p => p.Id == request.Id);

            if(player == null)
            {
                throw new NotFoundException(nameof(Player), request.Id);
            }

            _dbContext.Players.Remove(player);
            await _dbContext.SaveChangesAsync();

            return Unit.Value;
        }
    }
}
