﻿using Application.Common.Interfaces;
using Application.Players.CustomValidators;
using FluentValidation;

namespace Application.Players.Commands.CreatePlayer
{
    public class CreatePlayerCommandValidator : AbstractValidator<CreatePlayerCommand>
    {
        public CreatePlayerCommandValidator()
        {
            RuleFor(player => player)
                .SetValidator(new PlayerInfoValidator());
        }
    }
}
