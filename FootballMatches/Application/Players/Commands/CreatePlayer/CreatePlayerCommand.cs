﻿using Application.Common.CQRS;
using Application.Common.Interfaces;
using Application.Common.Models;
using Application.Common.Security;
using Domain.Entities;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Players.Commands.CreatePlayer
{
    [Authorize(Roles = "Administrator")]
    public class CreatePlayerCommand : PlayerInfo, IRequest<Guid>
    {
    }

    public class CreatePlayerCommandHandler : BaseCRUDHandler, IRequestHandler<CreatePlayerCommand, Guid>
    {
        public CreatePlayerCommandHandler(IApplicationDbContext dbContext) 
            : base(dbContext)
        {
        }

        public async Task<Guid> Handle(CreatePlayerCommand request, CancellationToken cancellationToken)
        {
            Player player = new()
            {
                Id = Guid.NewGuid(),
                Name = request.Name,
                Number = request.Number
            };

            await _dbContext.Players.AddAsync(player);
            await _dbContext.SaveChangesAsync();

            return player.Id;
        }
    }
}
