﻿using Application.Common.CQRS;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Application.Common.Models;
using Application.Common.Security;
using Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Players.Commands.UpdatePlayer
{
    [Authorize(Roles = "Administrator")]
    public class UpdatePlayerCommand : PlayerInfo, IRequest
    {
        public Guid Id { get; set; }
    }

    public class UpdatePlayerCommandHandler : BaseCRUDHandler, IRequestHandler<UpdatePlayerCommand>
    {
        public UpdatePlayerCommandHandler(IApplicationDbContext dbContext) 
            : base(dbContext)
        {
        }

        public async Task<Unit> Handle(UpdatePlayerCommand request, CancellationToken cancellationToken)
        {
            Player player = await _dbContext.Players
                .FirstOrDefaultAsync(p => p.Id == request.Id);

            if (player == null)
            {
                throw new NotFoundException(nameof(Player), request.Id);
            }

            player.Name = request.Name;
            player.Number = request.Number;

            await _dbContext.SaveChangesAsync();

            return Unit.Value;
        }
    }
}
