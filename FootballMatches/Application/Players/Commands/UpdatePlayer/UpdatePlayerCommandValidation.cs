﻿using Application.Players.CustomValidators;
using FluentValidation;

namespace Application.Players.Commands.UpdatePlayer
{
    public class UpdatePlayerCommandValidation : AbstractValidator<UpdatePlayerCommand>
    {
        public UpdatePlayerCommandValidation()
        {
            RuleFor(player => player)
                 .SetValidator(new PlayerInfoValidator());
        }
    }
}
