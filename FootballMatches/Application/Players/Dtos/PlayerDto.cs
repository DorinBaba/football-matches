﻿using System;

namespace Application.Players.Dtos
{
    public class PlayerDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Number { get; set; }
    }
}
