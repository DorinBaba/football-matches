﻿using Application.Teams.Dtos;
using System.Collections.Generic;

namespace Application.Players.Dtos
{
    public class PlayerWithTeamsDto : PlayerDto
    {
        public ICollection<TeamDto> Teams { get; set; }
    }
}
