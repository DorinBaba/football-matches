﻿using Application.Common.Models;
using FluentValidation;

namespace Application.Players.CustomValidators
{
    internal class PlayerInfoValidator : AbstractValidator<PlayerInfo>
    {
        public PlayerInfoValidator()
        {
            RuleFor(p => p.Name)
                .SetValidator(new PlayerNameValidator());

            RuleFor(p => p.Number)
                .SetValidator(new PlayerNumberValidator());
        }
    }
}
