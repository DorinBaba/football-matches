﻿using FluentValidation;

namespace Application.Players.CustomValidators
{
    internal class PlayerNumberValidator : AbstractValidator<int>
    {
        public PlayerNumberValidator()
        {
            RuleFor(number => number)
                .GreaterThanOrEqualTo(0)
                .LessThanOrEqualTo(99)
                .NotEmpty();
        }
    }
}
