﻿using FluentValidation;

namespace Application.Players.CustomValidators
{
    internal class PlayerNameValidator : AbstractValidator<string>
    {
        public PlayerNameValidator()
        {
            RuleFor(name => name)
               .Length(3, 200)
               .NotEmpty();
        }
    }
}
