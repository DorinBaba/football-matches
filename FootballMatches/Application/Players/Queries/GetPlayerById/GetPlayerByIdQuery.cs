﻿using Application.Common.CQRS;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Application.Players.Dtos;
using Domain.Entities;
using MapsterMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Players.Queries.GetPlayerById
{
    public class GetPlayerByIdQuery : IRequest<PlayerDto>
    {
        public Guid Id { get; set; }
    }

    public class GetPlayerByIdQueryHandler : BaseCRUDHandler, IRequestHandler<GetPlayerByIdQuery, PlayerDto>
    {
        readonly IMapper _mapper;

        public GetPlayerByIdQueryHandler(IApplicationDbContext dbContext, IMapper mapper)
            : base(dbContext)
        {
            _mapper = mapper;
        }

        public async Task<PlayerDto> Handle(GetPlayerByIdQuery request, CancellationToken cancellationToken)
        {
            var player = await _dbContext.Players
                .FirstOrDefaultAsync(p => p.Id == request.Id);

            if(player == null)
            {
                throw new NotFoundException(nameof(Player), request.Id);
            }

            return _mapper.Map<PlayerDto>(player);
        }
    }
}
