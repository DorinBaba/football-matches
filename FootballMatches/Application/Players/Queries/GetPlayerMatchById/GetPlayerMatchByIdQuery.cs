﻿using Application.Common.CQRS;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Application.Matches.Dtos;
using Domain.Entities;
using MapsterMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Players.Queries.GetPlayerMatchById
{
    public class GetPlayerMatchByIdQuery : IRequest<MatchDto>
    {
        public Guid PlayerId { get; set; }
        public Guid MatchId { get; set; }
    }

    public class GetPlayerMatchByIdQueryHandler : BaseCRUDHandler, IRequestHandler<GetPlayerMatchByIdQuery, MatchDto>
    {
        readonly IMapper _mapper;

        public GetPlayerMatchByIdQueryHandler(IApplicationDbContext dbContext, IMapper mapper) 
            : base(dbContext)
        {
            _mapper = mapper;
        }

        public async Task<MatchDto> Handle(GetPlayerMatchByIdQuery request, CancellationToken cancellationToken)
        {
            var player = await _dbContext.Players
                .FirstOrDefaultAsync(p => p.Id == request.PlayerId);

            if (player == null)
            {
                throw new NotFoundException(nameof(Player), request.PlayerId);
            }

            var match = await _dbContext.Matches
                .Include(m => m.Team1).ThenInclude(t => t.Players)
                .Include(m => m.Team2).ThenInclude(t => t.Players)
                .Include(m => m.Location)
                .FirstOrDefaultAsync(
                    m => m.Id == request.MatchId
                    && (m.Team1.Players.Contains(player) || m.Team2.Players.Contains(player)));

            return _mapper.Map<MatchDto>(match);
        }
    }

}
