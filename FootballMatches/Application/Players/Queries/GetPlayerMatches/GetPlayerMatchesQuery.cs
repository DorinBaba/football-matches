﻿using Application.Common.CQRS;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Application.Matches.Dtos;
using Domain.Entities;
using MapsterMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Players.Queries.GetPlayerMatches
{
    public class GetPlayerMatchesQuery : IRequest<IEnumerable<MatchDto>>
    {
        public Guid PlayerId { get; set; }
    }

    public class GetPlayerMatchesQueryHandler : BaseCRUDHandler, IRequestHandler<GetPlayerMatchesQuery, IEnumerable<MatchDto>>
    {
        readonly IMapper _mapper;

        public GetPlayerMatchesQueryHandler(IApplicationDbContext dbContext, IMapper mapper) 
            : base(dbContext)
        {
            _mapper = mapper;
        }

        public async Task<IEnumerable<MatchDto>> Handle(GetPlayerMatchesQuery request, CancellationToken cancellationToken)
        {
            var player = await _dbContext.Players
                .FirstOrDefaultAsync(p => p.Id == request.PlayerId);

            if(player == null)
            {
                throw new NotFoundException(nameof(Player), request.PlayerId);
            }

            var matches = await _dbContext.Matches
                .Include(m => m.Team1).ThenInclude(t => t.Players)
                .Include(m => m.Team2).ThenInclude(t => t.Players)
                .Include(m => m.Location)
                .Where(m => m.Team1.Players.Contains(player) || m.Team2.Players.Contains(player))
                .ToListAsync();

            return _mapper.Map<List<MatchDto>>(matches);
        }
    }
}
