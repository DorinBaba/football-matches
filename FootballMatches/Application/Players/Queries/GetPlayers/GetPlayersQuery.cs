﻿using Application.Common.CQRS;
using Application.Common.Interfaces;
using Application.Players.Dtos;
using MapsterMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Players.Queries.GetPlayers
{
    public class GetPlayersQuery : IRequest<IEnumerable<PlayerDto>>
    {

    }

    public class GetPlayersQueryHandler : BaseCRUDHandler, IRequestHandler<GetPlayersQuery, IEnumerable<PlayerDto>>
    {
        readonly IMapper _mapper;

        public GetPlayersQueryHandler(IApplicationDbContext dbContext, IMapper mapper)
            : base(dbContext)
        {
            _mapper = mapper;
        }

        public async Task<IEnumerable<PlayerDto>> Handle(GetPlayersQuery request, CancellationToken cancellationToken)
        {
            var players = await _dbContext.Players.ToListAsync();
            return _mapper.Map<IEnumerable<PlayerDto>>(players);
        }
    }
}
