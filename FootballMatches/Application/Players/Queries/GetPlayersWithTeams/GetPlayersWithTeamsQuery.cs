﻿using Application.Common.CQRS;
using Application.Common.Interfaces;
using Application.Players.Dtos;
using MapsterMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Players.Queries.GetPlayersWithTeams
{
    public class GetPlayersWithTeamsQuery : IRequest<IEnumerable<PlayerWithTeamsDto>>
    {
        public Guid Id { get; set; }
    }

    public class GetPlayersWithTeamsQueryHandler : BaseCRUDHandler, IRequestHandler<GetPlayersWithTeamsQuery, IEnumerable<PlayerWithTeamsDto>>
    {
        readonly IMapper _mapper;
     
        public GetPlayersWithTeamsQueryHandler(IApplicationDbContext dbContext, IMapper mapper) 
            : base(dbContext)
        {
            _mapper = mapper;
        }

        public async Task<IEnumerable<PlayerWithTeamsDto>> Handle(GetPlayersWithTeamsQuery request, CancellationToken cancellationToken)
        {
            var players = await _dbContext.Players
                .Include(p => p.Teams).AsNoTracking()
                .ToListAsync();

            return _mapper.Map<IEnumerable<PlayerWithTeamsDto>>(players);
        }
    }
}
