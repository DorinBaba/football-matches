﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Application.Common.Interfaces
{
    public interface IApplicationDbContext
    {
        DbSet<Match> Matches { get; set; }
        DbSet<Team> Teams { get; set; }
        DbSet<Player> Players { get; set; }
        DbSet<Location> Locations { get; set; }

        Task<int> SaveChangesAsync();
    }
}
