﻿using Application.Common.Interfaces;

namespace Application.Common.CQRS
{
    public abstract class BaseCRUDHandler
    {
        protected readonly IApplicationDbContext _dbContext;

        public BaseCRUDHandler(IApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }
    }
}
