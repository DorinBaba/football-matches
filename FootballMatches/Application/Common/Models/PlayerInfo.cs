﻿namespace Application.Common.Models
{
    /// <summary>
    /// Common data across multiple player-related commands
    /// </summary>
    public class PlayerInfo
    {
        public string Name { get; set; }
        public int Number { get; set; }
    }
}
