﻿using System;

namespace Application.Common.Models
{
    /// <summary>
    /// Common data across multiple match-related commands
    /// </summary>
    public class MatchInfo
    {
        public Guid Team1Id { get; set; }
        public int Team1Score { get; set; }
        public Guid Team2Id { get; set; }
        public int Team2Score { get; set; }
        public DateTime StartTime { get; set; }
        public int Minutes { get; set; }
    }
}
