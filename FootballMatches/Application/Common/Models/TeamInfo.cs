﻿namespace Application.Common.Models
{
    /// <summary>
    /// Common data across multiple team-related commands
    /// </summary>
    public class TeamInfo
    {
        public string Name { get; set; }
    }
}
