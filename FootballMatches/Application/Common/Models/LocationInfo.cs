﻿namespace Application.Common.Models
{
    /// <summary>
    /// Common data across multiple location-related commands
    /// </summary>
    public class LocationInfo
    {
        public string Name { get; set; }
    }
}
