﻿using System;

namespace Application.Common.Exceptions
{
    class InvalidCredentialsException : Exception
    {
        public InvalidCredentialsException()
            : base()
        {
        }

        public InvalidCredentialsException(string message)
            : base(message)
        {
        }
    }
}
