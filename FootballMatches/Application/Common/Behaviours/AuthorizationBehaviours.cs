﻿using Application.Common.Exceptions;
using Application.Common.IdentityModels;
using Application.Common.Security;
using Domain.ExtensionMethods;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Common.Behaviours
{
    public class AuthorizationBehaviour<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
    {
        readonly IIdentityService _identityService;
        readonly ICurrentUserService _currentUserService;

        public AuthorizationBehaviour(IIdentityService identityService, ICurrentUserService currentUserService)
        {
            _identityService = identityService;
            _currentUserService = currentUserService;
        }
        
        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            var authorizeAttributes = request.GetType().GetCustomAttributes<AuthorizeAttribute>();

            if (authorizeAttributes.Any())
            {
                if (!IsAuthenticated())
                    throw new UnauthorizedAccessException();

                if(!await IsAuthorized(authorizeAttributes))
                    throw new ForbiddenAccessException();
            }

            return await next.Invoke();
        }

        private bool IsAuthenticated()
        {
            return _currentUserService.UserId != null;
        }

        private async Task<bool> IsAuthorized(IEnumerable<AuthorizeAttribute> attributes)
        {
            var requiredRoles = attributes.SelectMany(a => a.Roles?.Split(',', System.StringSplitOptions.RemoveEmptyEntries));

            if (requiredRoles.IsNullOrEmpty())
                return true;

            foreach(string role in requiredRoles)
            {
                if (await _identityService.IsInRoleAsync(_currentUserService.UserId, role))
                    return true;
            }

            return false;
        }
    }
}
