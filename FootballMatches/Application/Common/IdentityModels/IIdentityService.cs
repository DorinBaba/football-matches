﻿using Application.Common.Models;
using System.Threading.Tasks;

namespace Application.Common.IdentityModels
{
    public interface IIdentityService
    {
        Task<bool> IsInRoleAsync(string userId, string role);
    }
}
