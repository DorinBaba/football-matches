﻿namespace Application.Common.IdentityModels.Token
{
    public class TokenResponse
    {
        public TokenResponse(string id, string email, string role, string token)
        {
            Id = id;
            EmailAddress = email;
            Token = token;
            Role = role;
        }

        public string Id { get; set; }
        public string EmailAddress { get; set; }
        public string Token { get; set; }
        public string Role { get; set; }
    }
}
