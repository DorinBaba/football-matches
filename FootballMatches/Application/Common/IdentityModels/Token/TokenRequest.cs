﻿namespace Application.Common.IdentityModels.Token
{
    public class TokenRequest
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
