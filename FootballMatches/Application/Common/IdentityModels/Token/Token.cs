﻿namespace Application.Common.IdentityModels.Token
{
    public class Token
    {
        public string Secret { get; set; }

        public string Issuer { get; set; }
        public int ExpiryMinutes { get; set; }
    }
}
