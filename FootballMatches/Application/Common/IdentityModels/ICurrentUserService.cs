﻿namespace Application.Common.IdentityModels
{
    public interface ICurrentUserService
    {
        public string UserId { get; }
    }
}
