﻿using Application.Common.IdentityModels.Token;
using System.Threading.Tasks;

namespace Application.Common.IdentityModels
{
    public interface ITokenService
    {
        Task<TokenResponse> Authenticate(TokenRequest request);
    }
}
