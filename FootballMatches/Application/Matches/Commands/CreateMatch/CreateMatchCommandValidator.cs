﻿using Application.Common.Interfaces;
using Application.Matches.CustomValidators;
using FluentValidation;

namespace Application.Matches.Commands.CreateMatch
{
    public class CreateMatchCommandValidator : AbstractValidator<CreateMatchCommand>
    {
        public CreateMatchCommandValidator(IApplicationDbContext dbContext)
        {
            RuleFor(m => m)
                .SetValidator(new MatchInfoValidator(dbContext));
        }
    }
}
