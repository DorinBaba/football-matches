﻿using Application.Common.CQRS;
using Application.Common.Interfaces;
using Application.Common.Models;
using Application.Common.Security;
using Domain.Entities;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Matches.Commands.CreateMatch
{
    [Authorize(Roles = "Administrator")]
    public class CreateMatchCommand : MatchInfo, IRequest<Guid>
    {
    }

    public class CreateMatchCommandHandler : BaseCRUDHandler, IRequestHandler<CreateMatchCommand, Guid>
    {
        public CreateMatchCommandHandler(IApplicationDbContext dbContext) 
            : base(dbContext)
        {
        }

        public async Task<Guid> Handle(CreateMatchCommand request, CancellationToken cancellationToken)
        {
            Match match = new()
            {
                Id = Guid.NewGuid(),
                Team1Id = request.Team1Id,
                Team1Score = request.Team1Score,
                Team2Id = request.Team2Id,
                Team2Score = request.Team2Score,
                StartTime = request.StartTime,
                Minutes = request.Minutes
            };

            await _dbContext.Matches.AddAsync(match);
            await _dbContext.SaveChangesAsync();

            return match.Id;
        }
    }
}
