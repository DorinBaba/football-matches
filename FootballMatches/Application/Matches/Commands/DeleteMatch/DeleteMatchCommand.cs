﻿using Application.Common.CQRS;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Application.Common.Security;
using Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Matches.Commands.DeleteMatch
{
    [Authorize(Roles = "Administrator")]
    public class DeleteMatchCommand : IRequest
    {
        public Guid Id { get; set; }
    }

    public class DeleteMatchCommandHandler : BaseCRUDHandler, IRequestHandler<DeleteMatchCommand>
    {
        public DeleteMatchCommandHandler(IApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<Unit> Handle(DeleteMatchCommand request, CancellationToken cancellationToken)
        {
            Match match = await _dbContext.Matches
                .FirstOrDefaultAsync(m => m.Id == request.Id);

            if(match == null)
            {
                throw new NotFoundException(nameof(Match), request.Id);
            }

            _dbContext.Matches.Remove(match);
            await _dbContext.SaveChangesAsync();

            return Unit.Value;
        }
    }
}
