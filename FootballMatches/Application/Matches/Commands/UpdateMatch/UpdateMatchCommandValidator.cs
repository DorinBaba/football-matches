﻿using Application.Common.Interfaces;
using Application.Matches.CustomValidators;
using FluentValidation;

namespace Application.Matches.Commands.UpdateMatch
{
    public class UpdateMatchCommandValidator : AbstractValidator<UpdateMatchCommand>
    {
        public UpdateMatchCommandValidator(IApplicationDbContext dbContext)
        {
            RuleFor(m => m)
                 .SetValidator(new MatchInfoValidator(dbContext));
        }
    }
}
