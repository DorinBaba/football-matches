﻿using Application.Common.CQRS;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Application.Common.Models;
using Application.Common.Security;
using Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Matches.Commands.UpdateMatch
{
    [Authorize(Roles = "Administrator")]
    public class UpdateMatchCommand : MatchInfo,  IRequest
    {
        public Guid Id { get; set; }
    }

    public class UpdateMatchCommandHandler : BaseCRUDHandler, IRequestHandler<UpdateMatchCommand>
    {
        public UpdateMatchCommandHandler(IApplicationDbContext dbContext) 
            : base(dbContext)
        {
        }

        public async Task<Unit> Handle(UpdateMatchCommand request, CancellationToken cancellationToken)
        {
            Match match = await _dbContext.Matches
                .FirstOrDefaultAsync(m => m.Id == request.Id);

            if(match == null)
            {
                throw new NotFoundException(nameof(Match), request.Id);
            }

            match.Team1Id = request.Team1Id;
            match.Team1Score = request.Team1Score;
            match.Team2Id = request.Team2Id;
            match.Team2Score = request.Team2Score;
            match.StartTime = request.StartTime;
            match.Minutes = request.Minutes;

            await _dbContext.SaveChangesAsync();

            return Unit.Value;
        }
    }
}
