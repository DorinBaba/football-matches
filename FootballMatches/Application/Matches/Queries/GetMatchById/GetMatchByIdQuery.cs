﻿using Application.Common.CQRS;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Application.Matches.Dtos;
using Domain.Entities;
using MapsterMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Matches.Queries.GetMatchById
{
    public class GetMatchByIdQuery : IRequest<MatchDto>
    {
        public Guid Id { get; set; }
    }

    public class GetMatchByIdQueryHandler : BaseCRUDHandler, IRequestHandler<GetMatchByIdQuery, MatchDto>
    {
        readonly IMapper _mapper;

        public GetMatchByIdQueryHandler(IApplicationDbContext dbContext, IMapper mapper)
            : base(dbContext)
        {
            _mapper = mapper;
        }

        public async Task<MatchDto> Handle(GetMatchByIdQuery request, CancellationToken cancellationToken)
        {
            Match match = await _dbContext.Matches
                .Include(m => m.Team1).ThenInclude(t => t.Players)
                .Include(m => m.Team2).ThenInclude(t => t.Players)
                .Include(m => m.Location)
                .FirstOrDefaultAsync(m => m.Id == request.Id);

            if(match == null)
            {
                throw new NotFoundException(nameof(Match), request.Id);
            }

            return _mapper.Map<MatchDto>(match);
        }
    }
}
