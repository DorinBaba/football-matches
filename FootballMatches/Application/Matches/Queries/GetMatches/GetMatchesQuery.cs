﻿using Application.Common.CQRS;
using Application.Common.Helpers;
using Application.Common.Interfaces;
using Application.Matches.Dtos;
using Domain.Entities;
using MapsterMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Matches.Queries.GetMatches
{
    public class GetMatchesQuery : IRequest<IEnumerable<MatchDto>>
    {
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public Guid? Location { get; set; }
        public Guid? Team { get; set; }
        public Guid? Player { get; set; }
    }

    public class GetMatchesQueryHandler : BaseCRUDHandler, IRequestHandler<GetMatchesQuery, IEnumerable<MatchDto>>
    {
        readonly IMapper _mapper;

        public GetMatchesQueryHandler(IApplicationDbContext dbContext, IMapper mapper)
            : base(dbContext)
        {
            _mapper = mapper;
        }

        public async Task<IEnumerable<MatchDto>> Handle(GetMatchesQuery request, CancellationToken cancellationToken)
        {
            Expression<Func<Match, bool>> filterExp = null;

            // Build the filter expression

            // Filter by interval
            if (request.StartTime != null && request.EndTime != null)
                filterExp = filterExp.ConcatAnd(
                    m => m.StartTime >= request.StartTime
                    && (m.StartTime + TimeSpan.FromMinutes(m.Minutes)) <= request.EndTime);

            // Filter by team
            if (request.Team != null)
                filterExp = filterExp.ConcatAnd(
                    m => m.Team1Id == request.Team
                    || m.Team2Id == request.Team);

            // Filter by player
            if (request.Player != null)
                filterExp = filterExp.ConcatAnd(
                    m => m.Team1.Players.Any(p => p.Id == request.Player)
                    || m.Team2.Players.Any(p => p.Id == request.Player));

            var query = _dbContext.Matches
                .Include(m => m.Team1).ThenInclude(t => t.Players)
                .Include(m => m.Team2).ThenInclude(t => t.Players)
                .Include(m => m.Location);

            var matches = (filterExp == null)
                ? await query.ToListAsync()
                : await query.Where(filterExp).ToListAsync();

            return _mapper.Map<List<MatchDto>>(matches);
        }
    }
}
