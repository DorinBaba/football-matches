﻿using Application.Locations.Dtos;
using Application.Teams.Dtos;
using System;

namespace Application.Matches.Dtos
{
    public class MatchDto
    {
        public Guid Id { get; set; }
        public TeamWithPlayersDto Team1 { get; set; }
        public int Team1Score { get; set; }
        public TeamWithPlayersDto Team2 { get; set; }
        public int Team2Score { get; set; }
        public LocationDto Location { get; set; }
        public DateTime StartTime { get; set; }
        public string Minutes { get; set; }
    }
}
