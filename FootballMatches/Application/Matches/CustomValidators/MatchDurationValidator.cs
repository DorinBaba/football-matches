﻿using FluentValidation;

namespace Application.Matches.CustomValidators
{
    internal class MatchDurationValidator : AbstractValidator<int>
    {
        public MatchDurationValidator()
        {
            RuleFor(minutes => minutes)
               .GreaterThan(0)
               .NotEmpty();
        }
    }
}
