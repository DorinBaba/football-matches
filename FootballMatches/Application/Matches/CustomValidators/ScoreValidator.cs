﻿using FluentValidation;

namespace Application.Matches.CustomValidators
{
    internal class ScoreValidator : AbstractValidator<int>
    {
        public ScoreValidator()
        {
            RuleFor(score => score)
                .GreaterThanOrEqualTo(0)
                .NotEmpty();
        }
    }
}
