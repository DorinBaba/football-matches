﻿using Application.Common.Interfaces;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Matches.CustomValidators
{
    internal class TeamIdValidator : AbstractValidator<Guid>
    {
        readonly IApplicationDbContext _dbContext;

        public TeamIdValidator(IApplicationDbContext dbContext)
        {
            _dbContext = dbContext;

            RuleFor(id => id)
                .MustAsync(BeValidTeamId)
                    .WithMessage("Invalid Team Id provided")
                .NotEmpty();
        }

        private async Task<bool> BeValidTeamId(Guid teamId, CancellationToken cancellationToken)
        {
            return await _dbContext.Teams
                .AnyAsync(t => t.Id == teamId);
        }
    }
}
