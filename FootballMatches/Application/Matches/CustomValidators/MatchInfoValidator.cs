﻿using Application.Common.Interfaces;
using Application.Common.Models;
using FluentValidation;

namespace Application.Matches.CustomValidators
{
    internal class MatchInfoValidator : AbstractValidator<MatchInfo>
    {
        public MatchInfoValidator(IApplicationDbContext dbContext)
        {
            RuleFor(m => m.Team1Id)
                .SetValidator(new TeamIdValidator(dbContext));

            RuleFor(m => m.Team2Id)
                .SetValidator(new TeamIdValidator(dbContext));

            RuleFor(m => m.Team1Id)
                .NotEqual(m => m.Team2Id)
                .WithMessage("A match should be played by two different teams.");

            RuleFor(m => m.Team1Score)
                .SetValidator(new ScoreValidator());

            RuleFor(m => m.Team2Score)
                .SetValidator(new ScoreValidator());

            RuleFor(m => m.Minutes)
                 .SetValidator(new MatchDurationValidator());
        }
    }
}
