﻿using Application.Common.CQRS;
using Application.Common.Interfaces;
using Application.Common.Models;
using Domain.Entities;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Teams.Commands.CreateTeam
{
    public class CreateTeamCommand : TeamInfo, IRequest<Guid>
    {
    }

    public class CreateTeamCommandHandler : BaseCRUDHandler, IRequestHandler<CreateTeamCommand, Guid>
    {
        public CreateTeamCommandHandler(IApplicationDbContext dbContext) 
            : base(dbContext)
        {
        }

        public async Task<Guid> Handle(CreateTeamCommand request, CancellationToken cancellationToken)
        {
            Team team = new()
            {
                Id = Guid.NewGuid(),
                Name = request.Name
            };

            await _dbContext.Teams.AddAsync(team);
            await _dbContext.SaveChangesAsync();
            
            return team.Id;
        }
    }
}
