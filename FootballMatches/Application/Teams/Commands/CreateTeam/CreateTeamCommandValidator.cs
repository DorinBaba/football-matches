﻿using Application.Teams.CustomValidators;
using FluentValidation;

namespace Application.Teams.Commands.CreateTeam
{
    public class CreateTeamCommandValidator : AbstractValidator<CreateTeamCommand>
    {
        public CreateTeamCommandValidator()
        {
            RuleFor(t => t)
                .SetValidator(new TeamInfoValidator());
        }
    }
}
