﻿using Application.Common.CQRS;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Teams.Commands.DeleteTeam
{
    public class DeleteTeamCommand : IRequest
    {
        public Guid Id { get; set; }
    }

    public class DeleteTeamCommandHandler : BaseCRUDHandler, IRequestHandler<DeleteTeamCommand>
    {
        public DeleteTeamCommandHandler(IApplicationDbContext dbContext) 
            : base(dbContext)
        {
        }

        public async Task<Unit> Handle(DeleteTeamCommand request, CancellationToken cancellationToken)
        {
            Team team = await _dbContext.Teams
                .FirstOrDefaultAsync(d => d.Id == request.Id);

            if(team == null)
            {
                throw new NotFoundException(nameof(Team), request.Id);
            }

            _dbContext.Teams.Remove(team);
            await _dbContext.SaveChangesAsync();

            return Unit.Value;
        }
    }
}
