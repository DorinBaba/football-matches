﻿using Application.Common.CQRS;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Teams.Commands.EnrollPlayer
{
    public class EnrollPlayerCommand : IRequest
    {
        public Guid PlayerId { get; set; }
        public Guid TeamId { get; set; }
    }

    public class EnrollPlayerCommandHandler : BaseCRUDHandler, IRequestHandler<EnrollPlayerCommand>
    {
        public EnrollPlayerCommandHandler(IApplicationDbContext dbContext) 
            : base(dbContext)
        {
        }

        public async Task<Unit> Handle(EnrollPlayerCommand request, CancellationToken cancellationToken)
        {
            Player player = await _dbContext.Players
                .FirstOrDefaultAsync(p => p.Id == request.PlayerId);

            if (player == null)
                throw new NotFoundException(nameof(Player), request.PlayerId);

            Team team = await _dbContext.Teams
                .Include(t => t.Players)
                .FirstOrDefaultAsync(t => t.Id == request.TeamId);

            if (team == null)
                throw new NotFoundException(nameof(Team), request.TeamId);

            if (team.Players.Any(p => p.Id == request.PlayerId))
                throw new InvalidOperationException($"Player ({request.PlayerId}) is already enrolled in the specified team");

            team.Players.Add(player);
            await _dbContext.SaveChangesAsync();

            return Unit.Value;
        }
    }
}