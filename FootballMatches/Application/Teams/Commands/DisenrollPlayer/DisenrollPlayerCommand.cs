﻿using Application.Common.CQRS;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Teams.Commands.DisenrollPlayer
{
    public class DisenrollPlayerCommand : IRequest
    {
        public Guid TeamId { get; set; }
        public Guid PlayerId { get; set; }
    }

    public class DisenrollPlayerCommandHandler : BaseCRUDHandler, IRequestHandler<DisenrollPlayerCommand>
    {
        public DisenrollPlayerCommandHandler(IApplicationDbContext dbContext) 
            : base(dbContext)
        {
        }

        public async Task<Unit> Handle(DisenrollPlayerCommand request, CancellationToken cancellationToken)
        {
            Team team = await _dbContext.Teams
                .Include(t => t.Players)
                .FirstOrDefaultAsync(t => t.Id == request.TeamId);

            if (team == null)
                throw new NotFoundException(nameof(Team), request.TeamId);

            Player player = team.Players
                .FirstOrDefault(p => p.Id == request.PlayerId);

            if (player == null)
                throw new NotFoundException($"The specified player is not enrolled in {team.Name}", request.PlayerId);

            team.Players.Remove(player);
            await _dbContext.SaveChangesAsync();

            return Unit.Value;
        }
    }
}
