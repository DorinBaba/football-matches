﻿using Application.Teams.CustomValidators;
using FluentValidation;

namespace Application.Teams.Commands.UpdateTeam
{
    public class UpdateTeamCommandValidator : AbstractValidator<UpdateTeamCommand>
    {
        public UpdateTeamCommandValidator()
        {
            RuleFor(t => t)
               .SetValidator(new TeamInfoValidator());
        }
    }
}
