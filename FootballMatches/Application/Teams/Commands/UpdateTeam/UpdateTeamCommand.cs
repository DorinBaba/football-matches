﻿using Application.Common.CQRS;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Application.Common.Models;
using Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Teams.Commands.UpdateTeam
{
    public class UpdateTeamCommand : TeamInfo, IRequest
    {
        public Guid Id { get; set; }
    }

    public class UpdateTeamCommandHandler : BaseCRUDHandler, IRequestHandler<UpdateTeamCommand>
    {
        public UpdateTeamCommandHandler(IApplicationDbContext dbContext) 
            : base(dbContext)
        {
        }

        public async Task<Unit> Handle(UpdateTeamCommand request, CancellationToken cancellationToken)
        {
            Team team = await _dbContext.Teams
                .FirstOrDefaultAsync(t => t.Id == request.Id);

            if(team == null)
            {
                throw new NotFoundException(nameof(Team), request.Id);
            }

            team.Name = request.Name;
            await _dbContext.SaveChangesAsync();

            return Unit.Value;
        }
    }
}
