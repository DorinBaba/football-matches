﻿using Application.Common.CQRS;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Application.Matches.Dtos;
using Domain.Entities;
using MapsterMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Teams.Queries.GetTeamMatches
{
    public class GetTeamMatchesQuery : IRequest<IEnumerable<MatchDto>>
    {
        public Guid TeamId { get; set; }
    }

    public class GetTeamMatchesQueryHandler : BaseCRUDHandler, IRequestHandler<GetTeamMatchesQuery, IEnumerable<MatchDto>>
    {
        readonly IMapper _mapper;

        public GetTeamMatchesQueryHandler(IApplicationDbContext dbContext, IMapper mapper)
            : base(dbContext)
        {
            _mapper = mapper;
        }

        public async Task<IEnumerable<MatchDto>> Handle(GetTeamMatchesQuery request, CancellationToken cancellationToken)
        {
            var team = await _dbContext.Teams
                .Include("AttendedMatches1.Team1.Players")
                .Include("AttendedMatches1.Team2.Players")
                .Include("AttendedMatches2.Team1.Players")
                .Include("AttendedMatches2.Team2.Players")
                .Include("AttendedMatches1.Location")
                .Include("AttendedMatches2.Location")
                .FirstOrDefaultAsync(t => t.Id == request.TeamId);

            if(team == null)
            {
                throw new NotFoundException(nameof(Team), request.TeamId);
            }

            return _mapper.Map<List<MatchDto>>(team.AttendedMatches);
        }
    }
}
