﻿using Application.Common.CQRS;
using Application.Common.Interfaces;
using Application.Teams.Dtos;
using MapsterMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Teams.Queries.GetTeamsWithPlayers
{
    public class GetTeamsWithPlayersQuery : IRequest<IEnumerable<TeamWithPlayersDto>>
    {
    }

    public class GetTeamsWithPlayersQueryHandler : BaseCRUDHandler, IRequestHandler<GetTeamsWithPlayersQuery, IEnumerable<TeamWithPlayersDto>>
    {
        readonly IMapper _mapper;

        public GetTeamsWithPlayersQueryHandler(IApplicationDbContext dbContext, IMapper mapper)
            : base(dbContext)
        {
            _mapper = mapper;
        }

        public async Task<IEnumerable<TeamWithPlayersDto>> Handle(GetTeamsWithPlayersQuery request, CancellationToken cancellationToken)
        {
            var teams = await _dbContext.Teams
                .Include(t => t.Players)
                .ToListAsync();

            return _mapper.Map<List<TeamWithPlayersDto>>(teams);
        }
    }
}
