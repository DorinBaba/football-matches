﻿using Application.Common.CQRS;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Application.Teams.Dtos;
using Domain.Entities;
using MapsterMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Teams.Queries.GetTeamById
{
    public class GetTeamByIdQuery : IRequest<TeamWithPlayersDto>
    {
        public Guid Id { get; set; }
    }

    public class GetTeamByIdQueryHandler : BaseCRUDHandler, IRequestHandler<GetTeamByIdQuery, TeamWithPlayersDto>
    {
        readonly IMapper _mapper;

        public GetTeamByIdQueryHandler(IApplicationDbContext dbContext, IMapper mapper)
            : base(dbContext)
        {
            _mapper = mapper;
        }

        public async Task<TeamWithPlayersDto> Handle(GetTeamByIdQuery request, CancellationToken cancellationToken)
        {
            var team = await _dbContext.Teams
                .Include(t => t.Players)
                .FirstOrDefaultAsync(t => t.Id == request.Id);

            if(team == null)
            {
                throw new NotFoundException(nameof(Team), request.Id);
            }

            return _mapper.Map<TeamWithPlayersDto>(team);
        }
    }
}
