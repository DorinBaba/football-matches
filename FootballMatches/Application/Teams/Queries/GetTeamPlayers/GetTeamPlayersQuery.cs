﻿using Application.Common.CQRS;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Application.Players.Dtos;
using Domain.Entities;
using MapsterMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Teams.Queries.GetTeamPlayers
{
    public class GetTeamPlayersQuery : IRequest<IEnumerable<PlayerDto>>
    {
        public Guid TeamId { get; set; }
    }

    public class GetTeamPlayersQueryHandler : BaseCRUDHandler, IRequestHandler<GetTeamPlayersQuery, IEnumerable<PlayerDto>>
    {
        readonly IMapper _mapper;

        public GetTeamPlayersQueryHandler(IApplicationDbContext dbContext, IMapper mapper)
            : base(dbContext)
        {
            _mapper = mapper;
        }

        public async Task<IEnumerable<PlayerDto>> Handle(GetTeamPlayersQuery request, CancellationToken cancellationToken)
        {
            var team = await _dbContext.Teams
                .Include(t => t.Players)
                .FirstOrDefaultAsync(t => t.Id == request.TeamId);

            if(team == null)
            {
                throw new NotFoundException(nameof(Team), request.TeamId);
            }

            return _mapper.Map<List<PlayerDto>>(team.Players);
        }
    }
}
