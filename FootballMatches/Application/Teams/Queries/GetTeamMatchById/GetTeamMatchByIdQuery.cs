﻿using Application.Common.CQRS;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Application.Matches.Dtos;
using Domain.Entities;
using MapsterMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Teams.Queries.GetTeamMatchById
{
    public class GetTeamMatchByIdQuery : IRequest<MatchDto>
    {
        public Guid TeamId { get; set; }
        public Guid MatchId { get; set; }
    }

    public class GetTeamMatchByIdQueryHandler : BaseCRUDHandler, IRequestHandler<GetTeamMatchByIdQuery, MatchDto>
    {
        readonly IMapper _mapper;

        public GetTeamMatchByIdQueryHandler(IApplicationDbContext dbContext, IMapper mapper)
            : base(dbContext)
        {
            _mapper = mapper;
        }

        public async Task<MatchDto> Handle(GetTeamMatchByIdQuery request, CancellationToken cancellationToken)
        {
            var team = await _dbContext.Teams
                .Include("AttendedMatches1.Team1.Players")
                .Include("AttendedMatches1.Team2.Players")
                .Include("AttendedMatches2.Team1.Players")
                .Include("AttendedMatches2.Team2.Players")
                .Include("AttendedMatches1.Location")
                .Include("AttendedMatches2.Location")
                .FirstOrDefaultAsync(t => t.Id == request.TeamId);

            if (team == null)
            {
                throw new NotFoundException(nameof(Team), request.TeamId);
            }

            var match = team.AttendedMatches.FirstOrDefault(m => m.Id == request.MatchId);

            if (team == null)
            {
                throw new NotFoundException(nameof(Match), request.MatchId);
            }

            return _mapper.Map<MatchDto>(match);
        }
    }
}
