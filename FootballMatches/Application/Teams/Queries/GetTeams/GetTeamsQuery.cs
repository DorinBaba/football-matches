﻿using Application.Common.CQRS;
using Application.Common.Interfaces;
using Application.Teams.Dtos;
using MapsterMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Teams.Queries.GetTeams
{
    public class GetTeamsQuery : IRequest<IEnumerable<TeamDto>>
    {
    }

    public class GetTeamsQueryHandler : BaseCRUDHandler, IRequestHandler<GetTeamsQuery, IEnumerable<TeamDto>>
    {
        readonly IMapper _mapper;

        public GetTeamsQueryHandler(IApplicationDbContext dbContext, IMapper mapper)
            : base(dbContext)
        {
            _mapper = mapper;
        }

        public async Task<IEnumerable<TeamDto>> Handle(GetTeamsQuery request, CancellationToken cancellationToken)
        {
            var teams = await _dbContext.Teams
                .ToListAsync();

            return _mapper.Map<List<TeamDto>>(teams);
        }
    }
}
