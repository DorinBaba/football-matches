﻿using Application.Common.CQRS;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Application.Players.Dtos;
using Domain.Entities;
using MapsterMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Teams.Queries.GetTeamPlayerById
{
    public class GetTeamPlayerByIdQuery : IRequest<PlayerDto>
    {
        public Guid TeamId { get; set; }
        public Guid PlayerId { get; set; }
    }

    public class GetTeamPlayerByIdQueryHandler : BaseCRUDHandler, IRequestHandler<GetTeamPlayerByIdQuery, PlayerDto>
    {
        readonly IMapper _mapper;

        public GetTeamPlayerByIdQueryHandler(IApplicationDbContext dbContext, IMapper mapper)
            : base(dbContext)
        {
            _mapper = mapper;
        }

        public async Task<PlayerDto> Handle(GetTeamPlayerByIdQuery request, CancellationToken cancellationToken)
        {
            // The only reason why this query & handler exists is to make sure that the 
            // specific player is within the specified team.

            var team = await _dbContext.Teams
                .Include(t => t.Players)
                .FirstOrDefaultAsync(t => t.Id == request.TeamId);

            if (team == null)
            {
                throw new NotFoundException(nameof(Team), request.TeamId);
            }

            var player = team.Players
                .FirstOrDefault(p => p.Id == request.PlayerId);

            if(player == null)
            {
                throw new NotFoundException(nameof(Player), request.PlayerId);
            }

            return _mapper.Map<PlayerDto>(player);
        }
    }
}
