﻿using FluentValidation;

namespace Application.Teams.CustomValidators
{
    internal class TeamNameValidator : AbstractValidator<string>
    {
        public TeamNameValidator()
        {
            RuleFor(name => name)
               .MaximumLength(200)
               .NotEmpty();
        }
    }
}
