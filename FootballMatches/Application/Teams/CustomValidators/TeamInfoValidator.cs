﻿using Application.Common.Models;
using FluentValidation;

namespace Application.Teams.CustomValidators
{
    internal class TeamInfoValidator : AbstractValidator<TeamInfo>
    {
        public TeamInfoValidator()
        {
            RuleFor(t => t.Name)
                .SetValidator(new TeamNameValidator());
        }
    }
}
