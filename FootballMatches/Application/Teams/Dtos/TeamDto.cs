﻿using System;

namespace Application.Teams.Dtos
{
    public class TeamDto 
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
