﻿using Application.Players.Dtos;
using System.Collections.Generic;

namespace Application.Teams.Dtos
{
    public class TeamWithPlayersDto : TeamDto
    {
        public ICollection<PlayerDto> Players { get; set; }
    }
}
