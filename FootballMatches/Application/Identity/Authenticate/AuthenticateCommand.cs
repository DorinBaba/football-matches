﻿using Application.Common.Exceptions;
using Application.Common.IdentityModels;
using Application.Common.IdentityModels.Token;
using MapsterMapper;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Identity.Authenticate
{
    public class AuthenticateCommand : TokenRequest, IRequest<TokenResponse>
    {
    }

    public class CommandHandler : IRequestHandler<AuthenticateCommand, TokenResponse>
    {
        private readonly ITokenService _tokenService;
        private readonly IMapper _mapper;
        private readonly HttpContext _httpContext;
        
        public CommandHandler(
            ITokenService tokenService,
            IMapper mapper,
            IHttpContextAccessor httpContextAccessor)
        {
            this._tokenService = tokenService ?? throw new ArgumentNullException(nameof(tokenService));
            this._mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            this._httpContext = (httpContextAccessor != null) ? httpContextAccessor.HttpContext : throw new ArgumentNullException(nameof(httpContextAccessor));
        }

        public async Task<TokenResponse> Handle(AuthenticateCommand command, CancellationToken cancellationToken)
        {
            TokenResponse tokenResponse = await _tokenService.Authenticate(command);
            
            if (tokenResponse == null)
            {
                throw new InvalidCredentialsException();
            }

            return tokenResponse;
        }
    }
}
