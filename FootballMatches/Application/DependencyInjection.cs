﻿using Application.Common.Configurations;
using Microsoft.Extensions.DependencyInjection;

namespace Application
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddApplication(this IServiceCollection services)
        {
            services.AddMediatR();
            services.AddMapster();
            services.AddFluentValidation();

            return services;
        }
    }
}
