﻿using Application.Common.Interfaces;
using Application.Common.Models;
using FluentValidation;

namespace Application.Locations.CustomValidatos
{
    public class LocationInfoValidator : AbstractValidator<LocationInfo>
    {
        public LocationInfoValidator(IApplicationDbContext dbContext)
        {
            RuleFor(v => v.Name).SetValidator(new LocationNameValidator(dbContext));
        }
    }
}
