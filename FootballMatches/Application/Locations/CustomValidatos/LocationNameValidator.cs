﻿using Application.Common.Interfaces;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Locations.CustomValidatos
{
    internal class LocationNameValidator : AbstractValidator<string>
	{
		readonly IApplicationDbContext _dbContext;

        public LocationNameValidator(IApplicationDbContext dbContext)
        {
			_dbContext = dbContext;

            RuleFor(name => name)
                .MaximumLength(200)
                .MustAsync(BeUniqueLocationName)
                .WithMessage("The specified location already exists.")
                .NotEmpty();
        }

        public async Task<bool> BeUniqueLocationName(string locationName, CancellationToken cancellationToken)
        {
            return await _dbContext.Locations
                .AllAsync(l => l.Name != locationName);
        }
    }
}
