﻿using Application.Common.CQRS;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Application.Locations.Dtos;
using MapsterMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Locations.Queries.GetLocationById
{
    public class GetLocationByIdQuery : IRequest<LocationDto>
    {
        public Guid Id { get; set; }
    }

    public class GetLocationsByIdQueryHandler : BaseCRUDHandler, IRequestHandler<GetLocationByIdQuery, LocationDto>
    {
        readonly IMapper _mapper;

        public GetLocationsByIdQueryHandler(IApplicationDbContext dbContext, IMapper mapper) 
            : base(dbContext)
        {
            _mapper = mapper;
        }

        public async Task<LocationDto> Handle(GetLocationByIdQuery request, CancellationToken cancellationToken)
        {
            var location = await _dbContext.Locations
                .FirstOrDefaultAsync(l => l.Id == request.Id);

            if(location == null)
            {
                throw new NotFoundException(nameof(LocationDto), request.Id);
            }

            return _mapper.Map<LocationDto>(location);
        }
    }
}
