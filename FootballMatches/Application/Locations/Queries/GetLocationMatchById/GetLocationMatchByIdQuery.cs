﻿using Application.Common.CQRS;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Application.Matches.Dtos;
using Domain.Entities;
using MapsterMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Locations.Queries.GetLocationMatchById
{
    public class GetLocationMatchByIdQuery : IRequest<MatchDto>
    {
        public Guid LocationId { get; set; }
        public Guid MatchId { get; set; }
    }

    public class GetLocationMatchByIdQueryHandler : BaseCRUDHandler, IRequestHandler<GetLocationMatchByIdQuery, MatchDto>
    {
        readonly IMapper _mapper;

        public GetLocationMatchByIdQueryHandler(IApplicationDbContext dbContext, IMapper mapper) 
            : base(dbContext)
        {
            _mapper = mapper;
        }

        public async Task<MatchDto> Handle(GetLocationMatchByIdQuery request, CancellationToken cancellationToken)
        {
            var match = await _dbContext.Matches
                .Include(m => m.Team1).ThenInclude(t => t.Players)
                .Include(m => m.Team2).ThenInclude(t => t.Players)
                .Include(m => m.Location)
                .FirstOrDefaultAsync(m => m.Id == request.MatchId && m.LocationId == request.LocationId);

            if (match == null)
            {
                throw new NotFoundException(nameof(Match), request.LocationId);
            }

            return _mapper.Map<MatchDto>(match);
        }
    }
}
