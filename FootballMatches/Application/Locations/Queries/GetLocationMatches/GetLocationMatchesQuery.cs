﻿using Application.Common.CQRS;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Application.Matches.Dtos;
using Domain.Entities;
using MapsterMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Locations.Queries.GetLocationMatches
{
    public class GetLocationMatchesQuery : IRequest<IEnumerable<MatchDto>>
    {
        public Guid LocationId { get; set; }
    }

    public class GetLocationMatchesQueryHandler : BaseCRUDHandler, IRequestHandler<GetLocationMatchesQuery, IEnumerable<MatchDto>>
    {
        readonly IMapper _mapper;
        
        public GetLocationMatchesQueryHandler(IApplicationDbContext dbContext, IMapper mapper) 
            : base(dbContext)
        {
            _mapper = mapper;
        }

        public async Task<IEnumerable<MatchDto>> Handle(GetLocationMatchesQuery request, CancellationToken cancellationToken)
        {
            var location = await _dbContext.Locations
                .Include("Matches")
                .Include("Matches.Team1.Players")
                .Include("Matches.Team2.Players")
                .Include("Matches.Location")
                .Where(l => l.Id == request.LocationId)
                .ToListAsync();

            if(location == null)
            {
                throw new NotFoundException(nameof(Location), request.LocationId);
            }

            var matches = location.SelectMany(l => l.Matches).ToList();

            return _mapper.Map<List<MatchDto>>(matches);
        }
    }
}
