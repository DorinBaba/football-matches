﻿using Application.Common.CQRS;
using Application.Common.Interfaces;
using Application.Locations.Dtos;
using MapsterMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Locations.Queries.GetLocations
{
    public class GetLocationsQuery : IRequest<IEnumerable<LocationDto>>
    {

    }

    public class GetLocationsQueryHandler : BaseCRUDHandler, IRequestHandler<GetLocationsQuery, IEnumerable<LocationDto>>
    {
        readonly IMapper _mapper;

        public GetLocationsQueryHandler(IApplicationDbContext dbContext, IMapper mapper) 
            : base(dbContext)
        {
            _mapper = mapper;
        }

        public async Task<IEnumerable<LocationDto>> Handle(GetLocationsQuery request, CancellationToken cancellationToken)
        {
            var locations = await _dbContext.Locations.ToListAsync();
            return _mapper.Map<List<LocationDto>>(locations);
        }
    }
}
