﻿using Application.Common.CQRS;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Application.Common.Security;
using Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Locations.Commands.DeleteLocation
{
    [Authorize(Roles = "Administrator")]
    public class DeleteLocationCommand : IRequest
    {
        public Guid Id { get; set; }
    }

    public class DeleteLocationCommandHandler : BaseCRUDHandler, IRequestHandler<DeleteLocationCommand>
    {
        public DeleteLocationCommandHandler(IApplicationDbContext dbContext) 
            : base(dbContext)
        {
        }

        public async Task<Unit> Handle(DeleteLocationCommand request, CancellationToken cancellationToken)
        {
            var location = await _dbContext.Locations
                .Where(l => l.Id == request.Id)
                .SingleOrDefaultAsync(cancellationToken);

            if(location == null)
            {
                throw new NotFoundException(nameof(Location), request.Id);
            }

            _dbContext.Locations.Remove(location);
            await _dbContext.SaveChangesAsync();

            return Unit.Value;
        }
    }
}
