﻿using Application.Common.Interfaces;
using Application.Locations.CustomValidatos;
using FluentValidation;

namespace Application.Locations.Commands.UpdateLocation
{
    public class UpdateLocationCommandValidator : AbstractValidator<UpdateLocationCommand>
    {
        public UpdateLocationCommandValidator(IApplicationDbContext dbContext)
        {
            RuleFor(v => v.Name).SetValidator(new LocationNameValidator(dbContext));
        }
    }
}
