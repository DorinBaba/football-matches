﻿using Application.Common.CQRS;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Application.Common.Security;
using Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Locations.Commands.UpdateLocation
{
    [Authorize(Roles = "Administrator")]
    public class UpdateLocationCommand : IRequest<bool>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }

    public class UpdateLocationCommandHandler : BaseCRUDHandler, IRequestHandler<UpdateLocationCommand, bool>
    {
        public UpdateLocationCommandHandler(IApplicationDbContext dbContext) 
            : base(dbContext)
        {
        }

        public async Task<bool> Handle(UpdateLocationCommand request, CancellationToken cancellationToken)
        {
            var location = await _dbContext.Locations
                .FirstOrDefaultAsync(l => l.Id == request.Id);

            if(location == null)
            {
                throw new NotFoundException(nameof(Location), request.Id);
            }

            location.Name = request.Name;
            await _dbContext.SaveChangesAsync();
            
            return true;
        }
    }
}
