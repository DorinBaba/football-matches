﻿using Application.Common.CQRS;
using Application.Common.Interfaces;
using Application.Common.Models;
using Application.Common.Security;
using Domain.Entities;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Locations.Commands.CreateLocation
{
    [Authorize(Roles = "Administrator")]
    public class CreateLocationCommand : LocationInfo, IRequest<Guid>
    {
    }

    public class CreateLocationCommandHandler : BaseCRUDHandler, IRequestHandler<CreateLocationCommand, Guid>
    {
        public CreateLocationCommandHandler(IApplicationDbContext dbContext)
            : base(dbContext)
        {

        }

        public async Task<Guid> Handle(CreateLocationCommand request, CancellationToken cancellationToken)
        {
            var location = new Location()
            {
                Id = Guid.NewGuid(),
                Name = request.Name
            };

            await _dbContext.Locations.AddAsync(location);
            await _dbContext.SaveChangesAsync();

            return location.Id;
        }
    }
}
