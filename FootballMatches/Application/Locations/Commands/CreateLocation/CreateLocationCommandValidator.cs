﻿using Application.Common.Interfaces;
using Application.Locations.CustomValidatos;
using FluentValidation;

namespace Application.Locations.Commands.CreateLocation
{
    public class CreateLocationCommandValidator : AbstractValidator<CreateLocationCommand>
    {
        public CreateLocationCommandValidator(IApplicationDbContext dbContext)
        {
            RuleFor(location => location)
                .SetValidator(new LocationInfoValidator(dbContext));
        }
    }
}
