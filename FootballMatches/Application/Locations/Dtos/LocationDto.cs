﻿using System;

namespace Application.Locations.Dtos
{
    public class LocationDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
