﻿using Application.Common.IdentityModels;
using Microsoft.AspNetCore.Http;
using System.Linq;
using System.Security.Claims;

namespace WebAPI.Services
{
    public class CurrentUserService : ICurrentUserService
    {
        readonly IHttpContextAccessor _httpContextAccessor;

        public string UserId { get => _httpContextAccessor.HttpContext?.User?.FindFirstValue("UserId"); }

        public CurrentUserService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }
    }
}
