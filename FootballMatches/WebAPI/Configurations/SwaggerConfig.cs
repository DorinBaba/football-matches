﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Linq;

namespace WebAPI.Configurations
{
    public static class SwaggerConfig
    {
        public static IServiceCollection AddSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                string appTitle = "FootballMatchesApi";
                string description = "This API exposes a set of endpoints than can be used for performing " +
                "CRUD operations on Football Matches along with Locations where these are held, Football Teams and Players. " +
                "Also, there are sepparate endpoints for enrolling players into teams or disenrolling them. ";

                // Versioning
                c.SwaggerDoc("v1", new OpenApiInfo { 
                    Title = appTitle + "_V1", 
                    Version = "v1", 
                    Description = description 
                });

                c.SwaggerDoc("v1.1", new OpenApiInfo { 
                    Title = appTitle + "_V1.1", 
                    Version = "v1.1", 
                    Description = description 
                });

                c.ResolveConflictingActions(a => a.First());
                c.OperationFilter<AddVersionHeaderParameter>();

                // Bearer token authentication
                OpenApiSecurityScheme securityDefinition = new OpenApiSecurityScheme()
                {
                    Name = "Bearer",
                    BearerFormat = "JWT",
                    Scheme = "bearer",
                    Description = "Specify the authorization token.",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.Http,
                };
                c.AddSecurityDefinition("jwt_auth", securityDefinition);

                OpenApiSecurityScheme securityScheme = new OpenApiSecurityScheme()
                {
                    Reference = new OpenApiReference()
                    {
                        Id = "jwt_auth",
                        Type = ReferenceType.SecurityScheme
                    }
                };
                OpenApiSecurityRequirement securityRequirements = new OpenApiSecurityRequirement()
                {
                    {securityScheme, new string[] { }},
                };
                c.AddSecurityRequirement(securityRequirements);
            });

            return services;
        }
    }

    public class AddVersionHeaderParameter : IOperationFilter
    {
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            operation.Parameters.Add(new OpenApiParameter
            {
                Name = "X-Version",
                Required = false,
                In = ParameterLocation.Header,
                Description = "Api version"
            });
        }
    }
}
