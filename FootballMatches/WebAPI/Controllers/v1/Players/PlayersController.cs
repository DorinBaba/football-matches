﻿using Application.Players.Commands.CreatePlayer;
using Application.Players.Commands.DeletePlayer;
using Application.Players.Commands.UpdatePlayer;
using Application.Players.Queries.GetPlayerById;
using Application.Players.Queries.GetPlayers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace WebAPI.Controllers.v1.Players
{
    [ApiVersion("1.0")]
    public class PlayersController : ApiBaseController
    {
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var players = await Mediator.Send(new GetPlayersQuery());
            return Ok(players);
        }

        [HttpGet("{id}", Name = "GetPlayerById")]
        public async Task<IActionResult> GetById(Guid id)
        {
            GetPlayerByIdQuery query = new() { Id = id };
            var player = await Mediator.Send(query);
            return Ok(player);
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreatePlayerCommand command)
        {
            Guid playerId = await Mediator.Send(command);
            return CreatedAtRoute("GetPlayerById", new { id = playerId }, null);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateMatch(Guid id, UpdatePlayerCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            await Mediator.Send(command);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            DeletePlayerCommand command = new() { Id = id };
            await Mediator.Send(command);
            return NoContent();
        }
    }
}
