﻿
using Application.Players.Queries.GetPlayerMatchById;
using Application.Players.Queries.GetPlayerMatches;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace WebAPI.Controllers.v1.Players
{
    [ApiVersion("1.0")]
    [Route("api/players/{playerId}/matches")]
    public class PlayerMatchesController : ApiBaseController
    {
        [HttpGet]
        public async Task<IActionResult> Get(Guid playerId)
        {
            GetPlayerMatchesQuery query = new() { PlayerId = playerId };
            var matches = await Mediator.Send(query);

            return Ok(matches);
        }

        [HttpGet("{matchId}")]
        public async Task<IActionResult> GetById(Guid playerId, Guid matchId)
        {
            GetPlayerMatchByIdQuery query = new()
            {
                PlayerId = playerId,
                MatchId = matchId
            };

            var match = await Mediator.Send(query);

            return Ok(match);
        }
    }
}
