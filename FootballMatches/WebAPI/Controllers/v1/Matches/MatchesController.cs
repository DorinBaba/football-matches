﻿using Application.Matches.Commands.CreateMatch;
using Application.Matches.Commands.DeleteMatch;
using Application.Matches.Commands.UpdateMatch;
using Application.Matches.Queries.GetMatchById;
using Application.Matches.Queries.GetMatches;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace WebAPI.Controllers.v1.Matches
{
    [ApiVersion("1.0")]
    public class MatchesController : ApiBaseController
    {
        [HttpGet]
        public async Task<IActionResult> Get(
            DateTime? startTime,
            DateTime? endTime,
            Guid? locationId,
            Guid? teamId,
            Guid? playerId)
        {
            GetMatchesQuery query = new()
            {
                StartTime = startTime,
                EndTime = endTime,
                Location = locationId,
                Player = playerId,
                Team = teamId
            };

            var matches = await Mediator.Send(query);
            return Ok(matches);
        }

        [HttpGet("{id}", Name = "GetMatchById")]
        public async Task<IActionResult> GetById(Guid id)
        {
            GetMatchByIdQuery query = new()
            {
                Id = id
            };

            var match = await Mediator.Send(query);
            return Ok(match);
        }

        [HttpPost]
        public async Task<IActionResult> CreateMatch(CreateMatchCommand command)
        {
            Guid matchId = await Mediator.Send(command);
            return CreatedAtRoute("GetMatchById", new { id = matchId }, null);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateMatch(Guid id, UpdateMatchCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            await Mediator.Send(command);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMatch(Guid id)
        {
            await Mediator.Send(new DeleteMatchCommand { Id = id });

            return NoContent();
        }
    }
}
