﻿using Application.Locations.Commands.CreateLocation;
using Application.Locations.Commands.DeleteLocation;
using Application.Locations.Commands.UpdateLocation;
using Application.Locations.Queries.GetLocationById;
using Application.Locations.Queries.GetLocations;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace WebAPI.Controllers.v1.Locations
{
    [ApiVersion("1.0")]
    public class LocationsController : ApiBaseController
    {
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var locations = await Mediator.Send(new GetLocationsQuery());
            return Ok(locations);
        }

        [HttpGet("{id}", Name = "GetLocationById")]
        public async Task<IActionResult> GetById(Guid id)
        {
            GetLocationByIdQuery query = new() { Id = id };
            var location = await Mediator.Send(query);
            return Ok(location);
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreateLocationCommand command)
        {
            Guid locationId = await Mediator.Send(command);
            return CreatedAtRoute("GetLocationById", new { id = locationId }, null);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateMatch(Guid id, UpdateLocationCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            await Mediator.Send(command);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            DeleteLocationCommand command = new() { Id = id };
            await Mediator.Send(command);
            return NoContent();
        }
    }
}
