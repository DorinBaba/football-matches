﻿using Application.Locations.Queries.GetLocationMatchById;
using Application.Locations.Queries.GetLocationMatches;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace WebAPI.Controllers.v1.Locations
{
    [ApiVersion("1.0")]
    [Route("api/locations/{locationId}/matches")]
    public class LocationMatchesController : ApiBaseController
    {
        [HttpGet]
        public async Task<IActionResult> Get(Guid locationId)
        {
            GetLocationMatchesQuery query = new() { LocationId = locationId };
            var matches = await Mediator.Send(query);

            return Ok(matches);
        }

        [HttpGet("{matchId}")]
        public async Task<IActionResult> GetById(Guid locationId, Guid matchId)
        {
            GetLocationMatchByIdQuery query = new()
            {
                LocationId = locationId,
                MatchId = matchId
            };

            var match = await Mediator.Send(query);

            return Ok(match);
        }
    }
}
