﻿using Application.Teams.Commands.DisenrollPlayer;
using Application.Teams.Commands.EnrollPlayer;
using Application.Teams.Queries.GetTeamPlayerById;
using Application.Teams.Queries.GetTeamPlayers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace WebAPI.Controllers.v1.Teams
{
    [ApiVersion("1.0")]
    [Route("api/teams/{teamId}/players")]
    public class TeamPlayersController : ApiBaseController
    {
        [HttpGet]
        public async Task<IActionResult> Get(Guid teamId)
        {
            GetTeamPlayersQuery query = new() { TeamId = teamId };
            var players = await Mediator.Send(query);

            return Ok(players);
        }

        [HttpGet("{playerId}")]
        public async Task<IActionResult> GetById(Guid teamId, Guid playerId)
        {
            GetTeamPlayerByIdQuery query = new()
            {
                TeamId = teamId,
                PlayerId = playerId
            };

            var player = await Mediator.Send(query);
            return Ok(player);
        }

        [HttpPut("{playerId}")]
        public async Task<IActionResult> Enroll(Guid teamId, Guid playerId)
        {
            EnrollPlayerCommand command = new()
            {
                TeamId = teamId,
                PlayerId = playerId
            };

            await Mediator.Send(command);
            return NoContent();
        }

        [HttpDelete("{playerId}")]
        public async Task<IActionResult> Disenroll(Guid teamId, Guid playerId)
        {
            DisenrollPlayerCommand command = new()
            {
                TeamId = teamId,
                PlayerId = playerId
            };

            await Mediator.Send(command);
            return NoContent();
        }
    }
}
