﻿using Application.Teams.Commands.CreateTeam;
using Application.Teams.Commands.DeleteTeam;
using Application.Teams.Commands.UpdateTeam;
using Application.Teams.Queries.GetTeamById;
using Application.Teams.Queries.GetTeams;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace WebAPI.Controllers.v1.Teams
{
    [ApiVersion("1.0")]
    public class TeamsController : ApiBaseController
    {
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var teams = await Mediator.Send(new GetTeamsQuery());
            return Ok(teams);
        }

        [HttpGet("{id}", Name = "GetTeamById")]
        public async Task<IActionResult> GetById(Guid id)
        {
            GetTeamByIdQuery query = new() { Id = id };
            var team = await Mediator.Send(query);
            return Ok(team);
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreateTeamCommand command)
        {
            Guid teamId = await Mediator.Send(command);
            return CreatedAtRoute("GetTeamById", new { id = teamId }, null);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateMatch(Guid id, UpdateTeamCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            await Mediator.Send(command);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            DeleteTeamCommand command = new() { Id = id };
            await Mediator.Send(command);
            return NoContent();
        }
    }
}
