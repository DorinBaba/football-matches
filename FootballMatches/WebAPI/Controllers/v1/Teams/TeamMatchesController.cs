﻿using Application.Teams.Queries.GetTeamMatchById;
using Application.Teams.Queries.GetTeamMatches;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace WebAPI.Controllers.v1.Teams
{
    [ApiVersion("1.0")]
    [Route("api/teams/{teamId}/matches")]
    public class TeamMatchesController : ApiBaseController
    {
        [HttpGet]
        public async Task<IActionResult> Get(Guid teamId)
        {
            GetTeamMatchesQuery query = new() { TeamId = teamId };
            var matches = await Mediator.Send(query);

            return Ok(matches);
        }

        [HttpGet("{matchId}")]
        public async Task<IActionResult> GetById(Guid teamId, Guid matchId)
        {
            GetTeamMatchByIdQuery query = new()
            {
                TeamId = teamId,
                MatchId = matchId
            };

            var match = await Mediator.Send(query);

            return Ok(match);
        }
    }
}
