﻿using Application.Identity.Authenticate;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace WebAPI.Controllers.v1.Token
{
    [ApiVersion("1.0")]
    public class TokenController : ApiBaseController
    {
        [HttpPost("Authenticate")]
        public async Task<IActionResult> AuthenticateAsync([FromBody] AuthenticateCommand command)
        {
            var response = await Mediator.Send(command);
            return Ok(response);
        }
    }
}
