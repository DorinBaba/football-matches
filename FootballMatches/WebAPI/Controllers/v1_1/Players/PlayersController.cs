﻿using Application.Players.Queries.GetPlayersWithTeams;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace WebAPI.Controllers.v1_1.Players
{
    [ApiVersion("1.1")]
    public class PlayersController : ApiBaseController
    {
        // Unlike v1.0, the api/players endpoint returns the list of players along with
        // all the teams they're enrolled in.
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var query = new GetPlayersWithTeamsQuery();
            var players = await Mediator.Send(query);
            return Ok(players);
        }
    }
}
