﻿using Application.Teams.Queries.GetTeamsWithPlayers;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace WebAPI.Controllers.v1_1.Teams
{
    [ApiVersion("1.1")]
    public class TeamsController : ApiBaseController
    {
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var teams = await Mediator.Send(new GetTeamsWithPlayersQuery());
            return Ok(teams);
        }
    }
}
