using Application;
using Application.Common.IdentityModels;
using Application.Common.IdentityModels.Token;
using FluentValidation.AspNetCore;
using Infrastructure;
using Infrastructure.Persistance.Seed;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using WebAPI.Configurations;
using WebAPI.Filters;
using WebAPI.Services;

namespace WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddApplication();
            services.AddInfrastructure(Configuration);
            services.SetAuthentication(Configuration);
            services.SetAuthorization();

            services.AddControllersWithViews(options =>
                options.Filters.Add<ApiExeptionFilterAttribute>())
                .AddFluentValidation();

            services.AddControllers();
            services.AddHttpContextAccessor();

            services.Configure<Token>(Configuration.GetSection("Authentication:Token"));
            services.AddSingleton<ICurrentUserService, CurrentUserService>();

            services.AddSwagger();

            services.AddApiVersioning(options =>
            {
                options.ApiVersionReader = new HeaderApiVersionReader("X-Version");
                options.DefaultApiVersion = new ApiVersion(1, 0);
                options.AssumeDefaultVersionWhenUnspecified = true;
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                
                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "FootballMatchesApi_V1");
                    c.SwaggerEndpoint("/swagger/v1.1/swagger.json", "FootballMatchesApi_V1.1");
                });
                
                app.SeedDataAsync().Wait();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseApiVersioning();
        }
    }
}
