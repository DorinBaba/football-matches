﻿using AutoFixture;
using AutoFixture.AutoMoq;
using AutoFixture.Xunit2;

namespace SharedTestHelpers
{
    public class AutoDomainDataAttribute : AutoDataAttribute
    {
        public static IFixture FixtureFactory()
        {
            var f = new Fixture();
            // I like members of interfaces to be configured, so i set it her
            f.Customize(new AutoMoqCustomization { ConfigureMembers = true });
            //f.Behaviors.Remove(new ThrowingRecursionBehavior());
            f.Behaviors.Add(new OmitOnRecursionBehavior());
            return f;
        }

        public AutoDomainDataAttribute() : base(FixtureFactory) { }
    }
}
