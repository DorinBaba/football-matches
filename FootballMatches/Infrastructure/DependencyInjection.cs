﻿using Application.Common.IdentityModels;
using Application.Common.Interfaces;
using Infrastructure.Configurations;
using Infrastructure.Persistance;
using Infrastructure.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext(configuration);
            services.AddIdentity();
            
            services.AddScoped<ITokenService, TokenService>();
            services.AddTransient<IIdentityService, IdentityService>();
            services.AddScoped<IApplicationDbContext>(provider => provider.GetService<ApplicationDbContext>());
            
            return services;
        }
    }
}
