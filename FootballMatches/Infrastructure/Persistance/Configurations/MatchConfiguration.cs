﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistance.Configurations
{
    public class MatchConfiguration : IEntityTypeConfiguration<Match>
    {
        public void Configure(EntityTypeBuilder<Match> builder)
        {
            builder.Property(m => m.Team1Id).IsRequired();
            builder.Property(m => m.Team1Score).IsRequired();
            builder.Property(m => m.Team2Id).IsRequired();
            builder.Property(m => m.Team2Score).IsRequired();
            builder.Property(m => m.StartTime).IsRequired();
            builder.Property(m => m.Minutes).IsRequired();

            builder.HasOne(m => m.Team1)
                .WithMany(t => t.AttendedMatches1)
                .HasForeignKey(m => m.Team1Id)
                .OnDelete(DeleteBehavior.ClientCascade);

            builder.HasOne(m => m.Team2)
                .WithMany(t => t.AttendedMatches2)
                .HasForeignKey(m => m.Team2Id)
                .OnDelete(DeleteBehavior.ClientCascade);

            builder.HasOne(m => m.Location)
                .WithMany(l => l.Matches)
                .HasForeignKey(m => m.LocationId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            builder.Ignore(m => m.Players);
        }
    }
}
