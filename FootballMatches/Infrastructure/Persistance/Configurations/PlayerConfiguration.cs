﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistance.Configurations
{
    public class PlayerConfiguration : IEntityTypeConfiguration<Player>
    {
        public void Configure(EntityTypeBuilder<Player> builder)
        {
            builder.Property(p => p.Name).IsRequired();
            builder.Property(p => p.Number).IsRequired();

            builder.HasMany(p => p.Teams)
                .WithMany(t => t.Players);
        }
    }
}
