﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistance.Configurations
{
    public class TeamConfiguration : IEntityTypeConfiguration<Team>
    {
        public void Configure(EntityTypeBuilder<Team> builder)
        {
            builder.Property(t => t.Name).HasMaxLength(200).IsRequired();

            builder.HasMany(t => t.Players)
                .WithMany(p => p.Teams);

            builder.HasMany(t => t.AttendedMatches1)
                .WithOne(m => m.Team1)
                .HasForeignKey(m => m.Team1Id);

            builder.HasMany(t => t.AttendedMatches2)
                .WithOne(m => m.Team2)
                .HasForeignKey(m => m.Team2Id);

            builder.Ignore(t => t.AttendedMatches);
        }
    }
}
