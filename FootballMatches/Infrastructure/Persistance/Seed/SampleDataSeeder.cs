﻿using Application.Common.IdentityModels;
using Domain.Entities;
using Infrastructure.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Persistance
{
    internal static class SampleDataSeeder
    {
        public static async Task SeedAsync(ApplicationDbContext context, UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            List<Location> seededLocations = new();
            List<Player> seededPlayers = new();
            List<Team> seededTeams = new();

            if (!context.Locations.Any())
            {
                context.Locations.AddRange(new Location() {Name = "Chisinau" },
                new Location() { Name = "Tel Aviv"},
                new Location() { Name = "Barcelona" },
                new Location() { Name = "Costa Rica" });

                await context.SaveChangesAsync();
                seededLocations = await context.Locations.ToListAsync();
            }

            if (!context.Players.Any())
            {
                context.Players.AddRange(
                    new Player() { Name = "Drake Crawford", Number = 10 },
                    new Player() { Name = "August Forbes", Number = 12 },
                    new Player() { Name = "Keshawn Beck", Number = 13 },
                    new Player() { Name = "Madilynn Weiss", Number = 15 },
                    new Player() { Name = "Drkae Arneloes", Number = 15 },
                    new Player() { Name = "Vasile Versaci", Number = 15 });

                await context.SaveChangesAsync();
                seededPlayers = await context.Players.ToListAsync();
            }

            if (!context.Teams.Any())
            {
                var team1 = new Team()
                {
                    Name = "New York Mavericks"
                };

                team1.Players.Add(seededPlayers[0]);
                team1.Players.Add(seededPlayers[1]);
                team1.Players.Add(seededPlayers[2]);

                var team2 = new Team()
                {
                    Name = "New York Mavericks"
                };

                team2.Players.Add(seededPlayers[3]);
                team2.Players.Add(seededPlayers[4]);
                team2.Players.Add(seededPlayers[5]);

                context.Teams.AddRange(team1, team2);
                await context.SaveChangesAsync();
                seededTeams = await context.Teams.ToListAsync();
            }

            if (!context.Matches.Any())
            {
                var match = new Match()
                {
                    Team1Id = seededTeams[0].Id,
                    Team2Id = seededTeams[1].Id,
                    Minutes = 120,
                    LocationId = seededLocations[0].Id,
                    StartTime = DateTime.Now - TimeSpan.FromDays(300),
                    Team1Score = 2,
                    Team2Score = 1,
                };

                await context.Matches.AddAsync(match);
                await context.SaveChangesAsync();
            }

            if (!(await context.Roles.AnyAsync()))
            {
                await roleManager.CreateAsync(new IdentityRole(AppIdentityConstants.Roles.Administrator));
                await roleManager.CreateAsync(new IdentityRole(AppIdentityConstants.Roles.User));
            }

            if (!(await userManager.GetUsersInRoleAsync(AppIdentityConstants.Roles.Administrator)).Any())
            {
                string adminUserName = "super@dmin.com";
                var adminUser = new ApplicationUser
                {
                    UserName = adminUserName,
                    Email = adminUserName,
                };

                var result = await userManager.CreateAsync(adminUser, "P4ssword");
                adminUser = await userManager.FindByEmailAsync(adminUserName);
                await userManager.AddToRoleAsync(adminUser, AppIdentityConstants.Roles.Administrator);
            }
        }
    }
}