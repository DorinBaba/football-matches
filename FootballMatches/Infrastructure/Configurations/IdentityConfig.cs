﻿using Infrastructure.Identity;
using Infrastructure.Persistance;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure.Configurations
{
    internal static class IdentityConfig
    {
        public static IServiceCollection AddIdentity(this IServiceCollection services)
        {
            services.AddIdentity<ApplicationUser, IdentityRole>()
                  .AddUserManager<UserManager<ApplicationUser>>()
                  .AddDefaultTokenProviders()
                  .AddSignInManager<SignInManager<ApplicationUser>>()
                  .AddEntityFrameworkStores<ApplicationDbContext>();

            services.Configure<IdentityOptions>(
               options =>
               {
                   options.User.RequireUniqueEmail = true;
                   options.Password.RequiredLength = 6;
                   options.Password.RequireNonAlphanumeric = false;
               });

            return services;
        }
    }
}
