# Fotball Matches API
----
## About
It mainly operates with Fotball matches, teams, players and locations and exposes a set of endpoints used for performing CRUD operations upon these entities.

## Key points about the app
- Create, Read, Update and Delete Fotball matches, teams, players, locations.
- Data filtering
- Data validation
- Exeption handling
- Authentication and Authorization
- EF Core as ORM
- [*To do*]  Unit tests coverage
- API versioning
- API documentation via Swagger
- Separation of concerns
